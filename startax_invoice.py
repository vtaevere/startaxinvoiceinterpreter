import data_serializer as ds

while True:
    df = ds.Startax.open_invoice()
    arve = ds.Startax.create_invoice(df)
    ds.Universal.write_df_to_csv(arve)

    print(arve.info())
    choice = input("Convert another invoice? (y/n)")

    if choice.upper() == "N":
        break
