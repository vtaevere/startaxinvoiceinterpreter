import pandas as pd
from pandas import DataFrame
import tkinter as tk
from tkinter import filedialog


class Universal:
    pd.set_option('display.max_columns', None)

    @staticmethod
    def read_excel_to_dataframe(filepath):
        df = pd.read_csv(filepath, skiprows=19, header=None, names=['Tuotekoodi', 'Tuotenimi', 'Hinta', 'Kpl', 'Summa', 'Alv %', ''], delimiter=r'\;+', error_bad_lines=False, skip_blank_lines=True, engine='python')
        print("Converting " + str(filepath))
        return df

    @staticmethod
    def write_df_to_csv(df):
        DataFrame.to_csv(df, Universal.FileDialog.ask_saveas_file('Save As'), sep=';', index=False, header=False)

    class FileDialog:

        root = tk.Tk()
        root.withdraw()

        @staticmethod
        def ask_open_file(title_of_dialog):
            return tk.filedialog.askopenfile(title=title_of_dialog, defaultextension='.csv', filetypes=[('CSV files', '.csv')])

        @staticmethod
        def ask_saveas_file(title_of_dialog):
            return tk.filedialog.asksaveasfilename(title=title_of_dialog, defaultextension='.csv', filetypes=[('CSV files', '.csv')])


class Startax:

    @staticmethod
    def open_invoice():
        df = Universal.read_excel_to_dataframe(Universal.FileDialog.ask_open_file('Open'))
        df.dropna(subset=['Alv %'], inplace=True)
        df['Hinta'] = df['Hinta'].str.replace('\xa0', '')  # Replace space character in number e.g '1 538.17e'
        df['Hinta'] = df['Hinta'].str.replace('e', '')
        df['Hinta'] = df['Hinta'].str.replace(',', '.')
        df['Hinta'] = df['Hinta'].astype('float')
        df['Km'] = df['Hinta'] * 1.2  # Add Estonian VAT
        return df

    @staticmethod
    def create_invoice(df):
        arve = df[['Tuotekoodi', 'Kpl', 'Km']].copy()  # AF format: product_code;qty;price_with_vat
        return arve
